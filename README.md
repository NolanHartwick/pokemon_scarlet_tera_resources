


# Overview
This repo exists to host some documents I built to help me do tera-lockes in gen 9. Tera-lockes are a type of nuzlocke that use tera raids as random encounters.

## Table of Contents

1. [Tera Lock Rules](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#tera-lock-rules)
2. [EVs and the level Cap](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#evs-and-the-level-cap)
3. [FAQ](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#faq)
4. [Resources - Quick Links](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#quick-links)
5. [Resources - Type Chart](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#type-chart)
6. [Resources - Tera Raid Encounters Table](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#tera-raid-encounters-table)
7. [Resources - Pinch Berries Table](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#pinch-berries-table)
8. [Pre Game Guide](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#pre-game-guide)
9. [Main Game Guide](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#main-game-guide)
10. [Late Game Guide](https://gitlab.com/NolanHartwick/pokemon_scarlet_tera_resources#late-game-guide)



## Tera lock rules
Tera lockes start at "Main Game", after the player has gotten the ride pokemon and the game opens up. Players must obey the following rules...

1. Players can only use tera raid pokemon (and their starter) for actual battles
2. Upon interacting with a tera raid, that raid is your encounter (possibly excepting optional rules). If you fail to beat the raid, you lose the encounter 
3. Players get 3 tera raid encounters before the first gym (plus their starter)
4. Players get 2 tera raid encounter after each badge (18 badges total)
5. (optional) Battle Item Clause : No healing items or X-items can be used in battle
6. (optional) lvl Cap Clause: each fight has a level cap. Pokemon above the cap can't be used
7. (optional) Hold Item Clause : each item can be on at most one of your pokemon in any given fight (basically no leftovers on everything)
8. (optional) Species Clause : same species encounters are rerolled
9. (optional) Tera Diversity Clause: No two team members can share a tera-type for any battle
10. (optional) No-Starter Clause: permanently box your starter in exchange for one extra tera raid encounter before the first gym
10. (optional) Max Raid Clause : raids that aren't closest (and below) your current cap are rerolled. The goal is to ensure all encoutners are potentially immediately usable while also maximizing the tera raid di*versity you encounter
    1. 1 star raids = lvl 12
    2. 2 star raids = lvl 20
    3. 3 star raids = lvl 35
    4. 4 star raids = lvl 45
    5. 5-6 star raids are locked to post game content, after the nuzlocke.

## EVs and the level cap
Because opponent pokemon are basically not EV trained, if you plan to EV train, this will give you a major stat advantage. This can be balanced by making your level caps a bit harder. I'd recomend the following...

EV_lvl_cap = lvl_cap - lvl_cap - // 15

Example: lvl_cap = 20, then EV_lvl_cap = 20 - 20 // 15 = 19

When this would cause back to back fights to have the same level cap, increase the level cap of the subsequent fight(s) until collision no longer occurs. The guide below includes both the base and ev trained level caps in the format: lvl_cap/EV_lvl_cap

## FAQ
1. Can I do terra raids to grind levels/items/whatever?
    * Yes, train however you want, but only use pokemon from the alloted encounters for your actual battles
2. Are utility pokemon allowed?
    * Yes, catch any pokemon you want, but only use pokemon from the alloted encounters for your actual battles. Some pokemon have useful out of battle abilities or useful mirror herb moves, have fun, just make sure you only use pokemon from alloted encounters for your actual battles. Keep these extra utility pokemon in a different box.
3. How do I reroll a tera raid encounter exactly?
    * You have two options, beat the encounter or fail it twice to clear it from the field. 
4. How do I respawn the raids?
    * Save your game and exit, go into your switch settings, dissable automatic time, and change the date on your switch, then jump back into the game
5. My run is dead, what should I do?
    *  You have two options, start a new run, or take a reset, ressurect all your fallen pokemon and continue. If you go the latter route, try to reset as few times as possible. If you practice and are careful, you will be completing runs without resets in no time
6. Why would you want to use the No-Starter Clause?
    * Two main reasons. First, it adds more variance to more runs since all encounters become randomized. This makes repeated runs more interesting. Second, the starters in Gen 9 are wildly overpowered. Their signature moves trivialize a massive portion of the game.
7. Why would you want to use optional clause X?
    * In general, the clauses are designed to increase the diversity of your encounters, your battles, and your nuzlocke runs in an effort to make the usable strategies more diverse and more interesting.

## Resources
### Quick Links
1. [Damage Calculator](https://calc.pokemonshowdown.com/)
2. [Interactive Map](https://mapgenie.io/pokemon-scarlet-violet/maps/paldea-region)

### Type Chart
![type-chart](https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Pokemon_Type_Chart.svg/492px-Pokemon_Type_Chart.svg.png)

### Tera Raid Encounters Table
| Name         | Level1 | Level2 | Level3 | Level4 |
|--------------|--------|--------|--------|--------|
| Pikachu |  | X | X |  |
| Raichu |  |  |  | X |
| Jigglypuff |  | X |  |  |
| Wigglytuff |  |  |  | X |
| Venonat | X |  |  |  |
| Venomoth |  |  | X |  |
| Diglett |  | X |  |  |
| Dugtrio |  |  |  | X |
| Meowth |  | X |  |  |
| Persian |  |  |  | X |
| Psyduck |  | X |  |  |
| Golduck |  |  |  | X |
| Mankey |  | X |  |  |
| Primeape |  |  |  | X |
| Growlithe |  | X |  |  |
| Slowpoke |  | X |  |  |
| Magnemite |  | X |  |  |
| Magneton |  |  |  | X |
| Grimer |  | X |  |  |
| Muk |  |  |  | X |
| Shellder | X | X |  |  |
| Cloyster |  |  |  | X |
| Gastly |  | X |  |  |
| Haunter |  |  | X |  |
| Drowzee | X |  |  |  |
| Hypno |  |  |  | X |
| Voltorb |  | X |  |  |
| Electrode |  |  |  | X |
| Chansey |  |  | X |  |
| Scyther |  |  |  | X |
| Tauros |  |  |  | X |
| Magikarp |  | X | X |  |
| Ditto |  |  | X | X |
| Eevee |  |  | X |  |
| Dratini |  |  | X |  |
| Dragonair |  |  |  | X |
| Pichu | X |  |  |  |
| Igglybuff | X |  |  |  |
| Mareep |  | X |  |  |
| Flaaffy |  |  | X |  |
| Ampharos |  |  |  | X |
| Marill |  |  | X |  |
| Azumarill |  |  |  | X |
| Sudowoodo |  |  | X |  |
| Hoppip | X |  |  |  |
| Skiploom |  |  | X |  |
| Sunkern | X |  |  |  |
| Sunflora |  |  | X |  |
| Wooper | X |  |  |  |
| Murkrow |  | X |  |  |
| Misdreavus |  | X |  |  |
| Girafarig |  |  | X |  |
| Pineco |  | X |  |  |
| Forretress |  |  |  | X |
| Dunsparce | X |  | X |  |
| Qwilfish |  |  | X |  |
| Heracross |  |  |  | X |
| Sneasel |  |  | X |  |
| Teddiursa |  | X |  |  |
| Ursaring |  |  |  | X |
| Houndour |  | X |  |  |
| Phanpy |  | X |  |  |
| Donphan |  |  |  | X |
| Stantler |  |  | X |  |
| Larvitar |  |  | X |  |
| Pupitar |  |  |  | X |
| Wingull | X |  |  |  |
| Pelipper |  |  | X |  |
| Ralts | X |  |  |  |
| Kirlia |  |  | X |  |
| Surskit | X |  |  |  |
| Masquerain |  |  |  | X |
| Shroomish | X |  |  |  |
| Slakoth |  | X |  |  |
| Vigoroth |  |  |  | X |
| Makuhita | X |  | X |  |
| Azurill | X |  |  |  |
| Sableye |  |  | X |  |
| Meditite |  | X |  |  |
| Medicham |  |  |  | X |
| Gulpin |  | X |  |  |
| Swalot |  |  |  | X |
| Numel |  |  | X |  |
| Torkoal |  |  |  | X |
| Spoink |  | X |  |  |
| Grumpig |  |  |  | X |
| Cacnea |  | X |  |  |
| Cacturne |  |  |  | X |
| Swablu | X |  |  |  |
| Zangoose |  |  | X | X |
| Seviper |  |  | X | X |
| Barboach | X |  |  |  |
| Whiscash |  |  |  | X |
| Shuppet |  | X |  |  |
| Banette |  |  |  | X |
| Tropius |  |  |  | X |
| Snorunt |  | X |  |  |
| Luvdisc |  | X |  |  |
| Bagon |  |  | X |  |
| Shelgon |  |  |  | X |
| Starly | X |  |  |  |
| Staravia |  |  | X |  |
| Kricketot | X |  |  |  |
| Kricketune |  |  | X |  |
| Shinx |  | X |  |  |
| Luxio |  |  | X |  |
| Combee | X |  |  |  |
| Vespiquen |  |  |  | X |
| Pachirisu |  |  | X |  |
| Buizel |  | X |  |  |
| Floatzel |  |  |  | X |
| Shellos |  | X |  |  |
| Gastrodon |  |  |  | X |
| Drifloon |  | X |  |  |
| Drifblim |  |  |  | X |
| Stunky |  | X |  |  |
| Skuntank |  |  |  | X |
| Bronzor |  |  | X |  |
| Bonsly | X |  |  |  |
| Gible |  |  | X |  |
| Gabite |  |  |  | X |
| Riolu |  |  | X |  |
| Hippopotas |  |  | X |  |
| Croagunk |  | X |  |  |
| Toxicroak |  |  |  | X |
| Finneon | X | X |  |  |
| Lumineon |  |  |  | X |
| Snover |  |  | X |  |
| Rotom |  |  |  | X |
| Petilil | X |  |  |  |
| Lilligant |  |  |  | X |
| Basculin |  |  | X |  |
| Sandile |  | X |  |  |
| Krokorok |  |  |  | X |
| Zorua |  |  | X |  |
| Gothita |  | X |  |  |
| Gothorita |  |  | X |  |
| Deerling | X |  |  |  |
| Sawsbuck |  |  |  | X |
| Foongus |  | X |  |  |
| Alomomola |  |  |  | X |
| Tynamo | X |  |  |  |
| Eelektrik |  |  |  | X |
| Axew |  |  | X |  |
| Fraxure |  |  |  | X |
| Cubchoo |  |  | X |  |
| Beartic |  |  |  | X |
| Cryogonal |  |  |  | X |
| Pawniard |  | X |  |  |
| Bisharp |  |  |  | X |
| Rufflet |  |  | X |  |
| Deino |  |  | X |  |
| Zweilous |  |  |  | X |
| Larvesta |  |  | X |  |
| Fletchling |  | X |  |  |
| Fletchinder |  |  | X |  |
| Litleo | X |  |  |  |
| Pyroar |  |  |  | X |
| FlabÃ©bÃ© | X |  |  |  |
| Skiddo | X |  |  |  |
| Gogoat |  |  |  | X |
| Skrelp |  | X |  |  |
| Clauncher |  | X |  |  |
| Hawlucha |  |  |  | X |
| Dedenne |  |  | X | X |
| Goomy |  |  | X |  |
| Sliggoo |  |  |  | X |
| Bergmite |  |  | X |  |
| Noibat |  |  | X |  |
| Yungoos | X |  |  |  |
| Gumshoos |  |  | X |  |
| Crabrawler | X |  |  |  |
| Crabominable |  |  |  | X |
| Oricorio |  |  |  | X |
| Rockruff | X |  |  |  |
| Lycanroc |  |  |  | X |
| Mareanie |  |  | X |  |
| Mudbray |  | X |  |  |
| Fomantis | X |  |  |  |
| Lurantis |  |  |  | X |
| Salandit |  | X |  |  |
| Bounsweet | X |  |  |  |
| Steenee |  |  | X |  |
| Oranguru |  |  | X |  |
| Passimian |  |  | X |  |
| Sandygast |  | X |  |  |
| Komala |  |  |  | X |
| Bruxish |  |  | X |  |
| Skwovet | X |  |  |  |
| Greedent |  |  | X |  |
| Rookidee | X |  |  |  |
| Corvisquire |  |  | X |  |
| Chewtle | X |  |  |  |
| Drednaw |  |  |  | X |
| Rolycoly |  | X |  |  |
| Carkol |  |  | X |  |
| Applin | X |  |  |  |
| Silicobra | X |  |  |  |
| Sandaconda |  |  |  | X |
| Arrokuda |  | X |  |  |
| Barraskewda |  |  |  | X |
| Toxel | X |  |  |  |
| Sinistea |  | X |  |  |
| Polteageist |  |  |  | X |
| Hatenna | X |  |  |  |
| Hattrem |  |  | X |  |
| Impidimp |  | X |  |  |
| Morgrem |  |  | X |  |
| Falinks |  |  |  | X |
| Pincurchin |  |  | X |  |
| Snom | X |  |  |  |
| Frosmoth |  |  |  | X |
| Stonjourner |  |  |  | X |
| Eiscue |  |  |  | X |
| Indeedee |  |  | X |  |
| Cufant |  | X |  |  |
| Dreepy |  |  | X |  |
| Drakloak |  |  |  | X |
| Lechonk | X |  |  |  |
| Oinkologne |  |  | X |  |
| Nymble | X |  |  |  |
| Lokix |  |  | X |  |
| Pawmi | X |  |  |  |
| Pawmo |  |  | X |  |
| Tandemaus |  | X |  |  |
| Maushold |  |  |  | X |
| Fidough | X |  |  |  |
| Dachsbun |  |  |  | X |
| Smoliv | X |  |  |  |
| Dolliv |  |  | X |  |
| Squawkabilly |  |  |  | X |
| Nacli | X |  |  |  |
| Naclstack |  |  | X |  |
| Charcadet | X |  | X |  |
| Tadbulb | X |  |  |  |
| Bellibolt |  |  |  | X |
| Wattrel | X |  |  |  |
| Kilowattrel |  |  |  | X |
| Maschiff | X |  |  |  |
| Shroodle | X |  |  |  |
| Grafaiai |  |  |  | X |
| Bramblin |  | X |  |  |
| Toedscool | X |  |  |  |
| Toedscruel |  |  |  | X |
| Capsakid |  | X |  |  |
| Scovillain |  |  |  | X |
| Rellor | X |  |  |  |
| Rabsca |  |  |  | X |
| Flittle | X |  |  |  |
| Espathra |  |  |  | X |
| Tinkatink |  | X |  |  |
| Tinkatuff |  |  | X |  |
| Wiglett | X |  |  |  |
| Wugtrio |  |  |  | X |
| Finizen |  |  | X |  |
| Varoom |  | X |  |  |
| Cyclizar |  |  |  | X |
| Glimmet |  |  | X |  |
| Greavard | X |  |  |  |
| Houndstone |  |  |  | X |
| Flamigo |  |  |  | X |
| Cetoddle |  |  | X |  |
| Veluza |  |  |  | X |
| Clodsire |  |  |  | X |
| Farigiraf |  |  |  | X |
| Dudunsparce |  |  |  | X |
| Frigibax |  |  | X |  |



### Pinch Berries Table
| Berry   | Stat         | Nature 1 | Nature 2 | Nature 3 | Nature 4 |
|---------|--------------|----------|----------|----------|----------|
| Figy    | Attack       | Lonely   | Adamant  | Naughty  | Brave    |
| Wiki    | Sp. Attack   | Modest   | Mild     | Rash     | Quiet    |
| Mago    | Speed        | Timid    | Hasty    | Jolly    | Naive    |
| Aguav   | Sp. Defence  | Calm     | Gentle   | Careful  | Sassy    |
| Iapapa  | Defence      | Bold     | Impish   | Lax      | Relaxed  |


Neutral natures always heal

# Pre Game Guide
Ends once player gets access to ride pokemon and the game opens up. Tera Raids don't start until after you get access to Koraidon or Miraidon as ride pokemon since this is when tera raids become available..


# Main Game Guide
Most Nuzlockes start here. Each of the following sections describes a single "fight" which may inlcude multiple individual battles when they occur immediately sequentially


## Gym 1 Bug Leader Katy - Lvl Cap 15/14
```
Nymble
Level: 14
Hardy Nature
Ability: Swarm
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Struggle Bug
- Double Kick

Tarountula
Level: 14
Hardy Nature
Ability: Insomnia
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Bug Bite
- Assurance

Teddiursa
Level: 15
Hardy Nature
Ability: Pickup
EVs: 252 HP
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Fury Cutter
- Fury Swipes
```

## Stony Cliff Titan - Lvl Cap 16/15
```
Klawf
Level: 16
Hardy Nature
Ability: Anger Shell
- Vise Grip
- Rock Smash
- Block
- Rock Tomb
```

## Gym 2 Grass Leader Brassius - Lvl Cap 17/16
Sunflora might attack during gym trial, be careful not to overlevel
```
Petilil
Level: 16
Hardy Nature
Ability: Own Tempo
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Sleep Powder
- Mega Drain

Smoliv
Level: 16
Hardy Nature
Ability: Early Bird
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Tackle
- Razor Leaf

Sudowoodo
Level: 17
Hardy Nature
Ability: Sturdy
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Trailblaze
- Rock Throw
```


## Team Star Dark Grunt - Lvl Cap 19/18
```
Murkrow
Level: 19
Hardy Nature
Ability: Insomnia
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Wing Attack
- Haze
- Gust
- Astonish
```

## Open Sky Titan - Lvl Cap 20/19
1v1 and 2v1 fights occur without a break
```
Bombirdier
Level: 20
Hardy Nature
Ability: Rocky Payload
- Rock Throw
- Wing Attack
- Pluck
- Torment
```

## Team Star Dark Boss Giacomo - Lvl Cap 21/20
```
Pawniard
Level: 21
Hardy Nature
Ability: Defiant
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Metal Claw
- Fury Cutter
- Aerial Ace

Segin Starmobile
Level: 20
Hardy Nature
Ability: Intimidate
- Wicked Torque
- Snarl
- Metal Sound
- Swift
```

## Nemona fight 3 - Lvl Cap 22/21
Happens when you enter a 3rd gym after having two gym badges
```
Rockruff
Level: 21
Hardy Nature
Ability: Vital Spirit
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Bite
- Howl
- Rock Throw
- Double Team

Pawmi
Level: 21
Hardy Nature
Ability: Static
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Bite
- Dig
- Nuzzle
- Charge

Floragato
Level: 22
Hardy Nature
Ability: Overgrow
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Quick Attack
- Magical Leaf
- Hone Claws
- Bite

Quaxwell
Level: 22
Hardy Nature
Ability: torrent
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Double Hit
- Water Pulse
- Wing Attack
- Work Up

Crocalor
Level: 22
Hardy Nature
Ability: Blaze
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Incinerate
- Yawn
- Bite
- Round
```

## Gym 3 Electric Trainers - Lvl Cap 23/22

#### Marti
```
Luxio
Level: 22
Hardy Nature
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Spark
- Bite
- Charge
- Thundershock
```

#### Michael
```
Tynamo
Level: 22
Hardy Nature
Ability: Levitate
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Charge Beam
- Spark
- Thunder Wave
- Tackle

Flaaffy
Level: 22
Hardy Nature
Ability: Static
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Take Down
- Charge
- Cotton Spore
- Thunder Wave
```

## Gym 3 Electric Leader Iono - Lvl Cap 24/23
```
Wattrel
Level: 23
Hardy Nature
Ability: Wind Power
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Pluck
- Quick Attack
- Spark

Bellibolt
Level: 23
Hardy Nature
Ability: Electromorphosis
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Water Gun
- Spark

Luxio
Level: 23
Hardy Nature
Ability: Intimidate
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Spark
- Bite

Mismagius
Level: 24
Hardy Nature
Ability: Levitate
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Charge Beam
- Hex
- Confuse Ray
```

## Team Star Fire Grunt A - Lvl Cap 25/24
```
Houndour
Level: 25
Hardy Nature
Ability: Early Bird
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Beat Up
- Incinerate
- Bite
- Roar
```

## Team Star Fire Boss Mela - Lvl Cap 27/26
```
Torkoal
Level: 27
Hardy Nature
Ability: Drought
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Flame Wheel
- Clear Smog

Schedar Starmobile
Level: 26
Hardy Nature
Ability: Speed Boost
- Overheat
- Blazing Torque
- Screech
- Swift
```

## Gym 4 Water Trainer Hugo - Lvl Cap 28/27
```
Floatzel
Level: 28
Hardy Nature
Ability: Swift Swim
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Aqua Jet
- Swift
- Bite
- Water Gun

Clauncher
Level: 28
Hardy Nature
Ability: Mega Launcher
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Hone Claws
- Smack Down
- Aqua Jet
- Flail
```

## Lurking Steel Titan - Lvl Cap 29/28
```
Orthowurm
Level: 29
Hardy Nature
Ability: Earth Eater
- Iron Tail
- Headbutt
- Wrap
- Sandstorm
```

## Gym 4 Water Leader Kofu - Lvl Cap 30/29
```
Veluza
Level: 29
Hardy Nature
Ability: Mold Breaker
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Slash
- Pluck
- Aqua Cutter

Wugtrio
Level: 29
Hardy Nature
Ability: Gooey
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Mud-Slap
- Water Pulse
- Headbutt

Crabominable
Level: 30
Hardy Nature
Ability: Iron Fist
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
EVs: 252 HP
- Crabhammer
- Rock Smash
- Slam
```

## Team Star Poison Trainer Youssef - Lvl Cap 31/30
```
Gulpin
Level: 30
Hardy Nature
Ability: Liquid Ooze
IVs: 15 HP / 15 Atk / 15 Def / 15 SpA / 15 SpD / 15 Spe
- Toxic
- Stockpile
- Spit Up
- Swallow

Shroodle
Level: 31
Hardy Nature
Ability: Unburden
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Poison Jab
- U-turn
- Slash
- Flatter
```

## Team Star Poison Boss Atticus - Lvl Cap 33/31
```
Skuntank
Level: 32
Hardy Nature
Ability: Stench
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Sucker Punch
- Toxic
- Venoshock

Muk
Level: 32
Hardy Nature
Ability: Stench
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 260 SpD / 20 Spe
- Sludge Wave
- Mud Slap

Revavroom
Level: 33
Hardy Nature
Ability: Overcoat
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Iron Head
- Sludge
- Assurance
- Bulldoze

Navi Starmobile
Level: 32
Hardy Nature
Ability: Toxic Debris
- Spin Out
- Noxious Torque
- Flame Charge
- Smog
```

## Gym 5 Normal Leader Larry + Nemona 4 - Lvl Cap 36/34

#### Larry
Secret Menu answers are: Grilled Rice Balls, Medium Serving, Extra crispy, Fire Blast style, Lemon
```
Komala
Level: 35
Hardy Nature
Ability: Comatose
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Yawn
- Sucker Punch
- Slam

Dudunsparce
Level: 35
Hardy Nature
Ability: Serene Grace
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Hyper Drill
- Drill Run
- Glare

Staraptor
Level: 36
Hardy Nature
Ability: Intimidate
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
EVs: 252 HP
- Facade
- Aerial Ace
```

#### Nemona Fight 4
Forced fight immediately after Larry
```
Lycanroc
Level: 36
Hardy Nature
Ability: Sand rush
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Quick Attack
- Accelerock
- Bite

Goomy
Level: 36
Hardy Nature
Ability: Sap Sipper
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Pulse
- Water Pulse
- Flail

Pawmo
Level: 36
Hardy Nature
Ability: Volt Absorb
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Spark
- Arm Thrust
- Thunder Wave
- Quick Attack

Meowscarada
Level: 37
Hardy Nature
Ability: Overgrow
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
EVs: 252 HP
- Slash
- Flower Trick
- Quick Attack

Quaquaval
Level: 37
Hardy Nature
Ability: Torrent
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
EVs: 252 HP
- Air Slash
- Aqua Step
- Aqua Jet

Skeledirge
Level: 37
Hardy Nature
Ability: Blaze
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
EVs: 252 HP
- Snarl
- Torch Song
- Lick
```

## Gym 6 Ghost Trainers - Lvl Cap 40/38
all fights are double battles and back to back with weird stat boosts

#### Gym Trainer Tas
```
Shuppet
Level: 40
Hardy Nature
Ability: Insomnia
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Sucker Punch
- Role Play
- Shadow Ball
- Curse

Greavard
Level: 40
Hardy Nature
Ability: Pickup
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Helping Hand
- Play Rough
- Crunch
- Rest
```

#### Gym Trainer Lani
```
Haunter
Level: 40
Hardy Nature
Ability: Levitate
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Sucker Punch
- Night Shade
- Hex
- Curse

Misdreavus
Level: 40
Hardy Nature
Ability: Levitate
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Payback
- Pain Split
- Psybeam
- Hex
```

#### Gym Trainer MC Sledge
```
Sableye
Level: 40
Hardy Nature
Ability: Keen Eye
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Power Gem
- Mean Look
- Shadow Claw
- Quash

Drifblim
Level: 40
Hardy Nature
Ability: Aftermath
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Destiny Bond
- Self-Destruct
- Stockpile
- Swallow
```

## Gym 6 Ghost Leader Rhyme - Lvl Cap 41/39
This fight is a double battle with weird stat boost mechanics.
```
Banette
Level: 41
Hardy Nature
Ability: Insomnia
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Icy Wind
- Sucker Punch
- Shadow Sneak

Mimikyu
Level: 41
Hardy Nature
Ability: Disguise
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Light Screen
- Shadow Sneak
- Slash

Houndstone
Level: 41
Hardy Nature
Ability: Sand Rush
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Play Rough
- Crunch
- Phantom Force

Toxtricity-Low-Key
Level: 42
Hardy Nature
Ability: Punk Rock
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Discharge
- Hex
- Hyper Voice
```

## Nemona Fight 5 - Level Cap 42/40
Starts upon entering 7th gym after having 6 gym badges
```
Lycanroc
Level: 42
Hardy Nature
Ability: Sand Rush
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Accelerock
- Crunch
- Rock Slide
- Sand Attack

Sliggoo
Level: 42
Hardy Nature
Ability: Sap Sipper
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Pulse
- Water Pulse
- Flail

Pawmot
Level: 42
Hardy Nature
Ability: Volt Absorb
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Spark
- Arm Thrust
- Thunder Wave
- Quick Attack

Skeledirge
Level: 43
Hardy Nature
Ability: Blaze
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Snarl
- Torch Song
- Lick

Meowscarada
Level: 43
Hardy Nature
Ability: Overgrow
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Slash
- Quick Attack
- Flower Trick

Quaquaval
Level: 43
Hardy Nature
Ability: Torrent
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Air Slash
- Aqua Step
- Aqua Jet
```

## Gym 7 Psychic Trainers - Lvl Cap 43/41
Fights are back to back

#### Emily
```
Gothorita
Level: 43
Hardy Nature
Ability: Competitive
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Psyshock
- Hypnosis
- Fake Tears
- Psych Up

Kirlia
Level: 43
Hardy Nature
Ability: Trace
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Heal Pulse
- Pyschic
- Charm
- Life Dew
```

#### Rafael
```
Grumpig
Level: 43
Hardy Nature
Ability: ?
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Psyshock
- Power Gem
- Snore
- Rest

Indeedee
Level: 43
Hardy Nature
Ability: ?
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Calm Mind
- Psychic
- Healing Wish
- After You

Medicham
Level: 43
Hardy Nature
Ability: Pure Power
IVs: 10 HP / 10 Atk / 10 Def / 10 SpA / 10 SpD / 10 Spe
- Power Trick
- Accupressure
- Psych Up
- High Jump Kick
```


## Gym 7 Psychic Leader Tulip - Lvl Cap 44/42
```
Farigiraf
Level: 44
Hardy Nature
Ability: Armor Tail
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Crunch
- Zen Headbutt
- Reflect

Gardevoir
Level: 44
Hardy Nature
Ability: Synchronize
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Psychic
- Dazzling Gleam
- Energy Ball

Espathra
Level: 44
Hardy Nature
Ability: Opportunist
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Psychic
- Shadow Ball
- Quick Attack

Florges
Level: 45
Hardy Nature
Ability: Flower Veil
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Psychic
- Moonblast
- Petal Blizzard
```

## Quaking Earth Titan - Lvl Cap 45/43
```
Great Tusk
Level: 45
Hardy Nature
Ability: Protosynthesis
- Rapid Spin
- Brick Break
- Knock Off
- Stomping Tantrum
```

## Gym 8 Ice Leader Grusha - Lvl Cap 48/45
```
Frosmoth
Level: 47
Hardy Nature
Ability: Shield Dust
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Blizzard
- Bugg Buzz
- Tailwind

Beartic
Level: 47
Hardy Nature
Ability: Snow Cloak
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Aqua Jet
- Earthquake
- Icicle Crash

Cetitan
Level: 47
Hardy Nature
Ability: Thick Fat
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Ice Spinner
- Liquidation
- Ice Shard

Altaria
Level: 48
Hardy Nature
Ability: Natural Cure
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Ice Beam
- Dragon Pulse
- Moon Blast
- Hurricane
```

## Team Star Fairy Grunt Harrington - Lvl Cap 49/46
```
Morgrem
Level: 48
Hardy Nature
Ability: Prankster
IVs: 15 HP / 15 Atk / 15 Def / 15 SpA / 15 SpD / 15 Spe
- Play Rough
- Nasty Plot
- Dark Pulse
- Torment

Hattrem
Level: 49
Hardy Nature
Ability: Healer
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Psychic
- Calm Mind
- Dazzling Gleam
- Heal Pulse
```


## Team Star Fairy Boss Ortega - Lvl Cap 51/48
```
Azumarill
Level: 50
Hardy Nature
Ability: Huge Power
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Aqua Tail
- Play Rough
- Bounce
- Charm

Wigglytuff
Level: 50
Hardy Nature
Ability: Cute Charm
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Body Slam
- Play Rough
- Gyro Ball
- Charm

Dachsbun
Level: 51
Hardy Nature
Ability: Well-Baked Body
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Crunch
- Play Rough
- Baby-Doll Eyes
- Mud-Slap

Ruchbah Starmobile
Level: 50
Hardy Nature
Ability: Misty Surge
- Steel Roller
- Magical Torque
- Confuse Ray
- Spin Out
```



## Team Star Fighting Grunt Carmen - Lvl Cap 54/51
```
Croagunk
Level: 54
Hardy Nature
Ability: Anticipation
- Belch
- Sludge Bomb
- Nasty Plot
- Toxic

Primeape
Level: 55
Hardy Nature
Ability: Vital Spirit
- Outrage
- Stomping Tantrum
- Screech
- Close Combat
```

##  Team Star Fighting Boss Eri - Lvl Cap 56/53
```
Toxicroak
Level: 55
Hardy Nature
Ability: Anticipation
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Poison Jab
- Brick Break
- Sucker Punch

Passimian
Level: 55
Hardy Nature
Ability: Receiver
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Close Combat
- Rock Tomb
- Seed Bomb

Lucario
Level: 55
Hardy Nature
Ability: Steadfast
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Pulse
- Aura Sphere
- Dark Pulse

Annihilape
Level: 56
Hardy Nature
Ability: Vital Spirit
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Rage Fist
- Close Combat
- Ice Punch
- Fire Punch

Caph Starmobile
Level: 56
Hardy Nature
Ability: Stamina
- Combat Torque
- Spin Out
- Shift Gear
- High Horsepower
```

## False Dragon Titan - Level Cap 57/54
Two dodonzo fights and one tatsugiri fight
```
Dondozo
Level: 56
Hardy Nature
Ability: Oblivious
- Aqua Tail
- Body Slam
- Knock Off
- Order Up

Tatsugiri
Level: 57
Hardy Nature
Ability: Commander
- Muddy Water
- Icy Wind
- Taunt
- Dragon Pulse
```

# Late Game Guide

Level caps get pretty screwed through the late game. What follows is the recomended order. If your pokemon are below the level cap, feel free to train them to the level cap. If they exceed the level cap because of experience gain, don't train them anymore, but feel free to use them.

## Pokemon League - Level Cap 58/55
You can take a break between fights to change items/moves/order but can't access the Box

#### Rika - Ground
```
Whiscash
Level: 57
Hardy Nature
Ability: Oblivious
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Muddy Water
- Earth Power
- Blizzard
- Future Sight

Camerupt
Level: 57
Hardy Nature
Ability: Magma Armor
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Earth Power
- Fire Blast
- Flash Cannon
- Yawn

Donphan
Level: 57
Hardy Nature
Ability: Sturdy
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Earthquake
- Stone Edge
- Iron Head
- Poison Jab

Dugtrio
Level: 57
Hardy Nature
Ability: Sand Veil
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Earthquake
- Rock slide
- Sucker Punch
- Sandstorm

Clodsire
Level: 58
Hardy Nature
Ability: Water Absorb
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Earthquake
- Liquidation
- Toxic
- Protect
```

#### Poppy - Steel
```
Copperajah
Level: 58
Hardy Nature
Ability: Sheer force
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- High Horsepower
- Play Rough
- Heavy Slam
- Stealth Rock

Magnezone
Level: 58
Hardy Nature
Ability: Sturdy
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Discharge
- Flash Cannon
- Light Screen
- Tri Attack

Bronzong
Level: 58
Hardy Nature
Ability: Levitate
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Iron Head
- Zen Headbutt
- Rock Blast
- Earthquake

Corviknight
Level: 58
Hardy Nature
Ability: Pressure
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Brave Bird
- Iron Head
- Body Press
- Iron Defense

Tinkaton
Level: 59
Hardy Nature
Ability: Mold Breaker
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Play Rough
- Gigaton Hammer
- Break Break
- Stone Edge
```

#### Larry - Flying
```
Tropius
Level: 59
Hardy Nature
Ability: Chlorophyll
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Air Slash
- Solar Beam
- Dragon Pulse
- Sunny Day

Oricorio-Pom-Pom
Level: 59
Hardy Nature
Ability: Dancer
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Revelation Dance
- Air Slash
- Teeter Dance
- Ivy Wind

Altaria
Level: 59
Hardy Nature
Ability: Natural Cure
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Moonblast
- Flamethrower
- Ice Beam
- Icy Wind

Staraptor
Level: 59
Hardy Nature
Ability: Intimidate
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Facade
- Brave Bird
- Close Combat
- Thief

Flamigo
Level: 60
Hardy Nature
Ability: Scrappy
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Brave Bird
- Close Combat
- Throat Chop
- Liquidation
```

#### Hassel - Dragon
```
Noivern
Level: 60
Hardy Nature
Ability: Infiltrator
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Air Slash
- Dragon Pulse
- Super Fang
- Hyper Voice

Haxorus
Level: 60
Hardy Nature
Ability: Mold Breaker
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Claw
- Crunch
- Iron Head
- Rock Tomb

Dragalge
Level: 60
Hardy Nature
Ability: Poison Point
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Sludge Bomb
- Dragon Pulse
- Hydro Pump
- Thunderbolt

Flapple
Level: 60
Hardy Nature
Ability: Ripen
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Rush
- Seed Bomb
- Aerial Ace
- Leech Seed

Baxcalibur
Level: 61
Hardy Nature
Ability: Thermal Exchange
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Icicle Crash
- Brick Break
- Glaive Rush
```

#### Geeta
```
Espathra
Level: 61
Hardy Nature
Ability: Opportunist
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Lumina Crash
- Dazzling Gleam
- Quick Attack
- Reflect

Gogoat
Level: 61
Hardy Nature
Ability: Sap Sipper
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Horn Leech
- Zen Headbutt
- Play Rough
- Bulk Up

Veluza
Level: 61
Hardy Nature
Ability: Mold Breaker
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Aqua Jet
- Liquidation
- Psycho Cut
- Ice Fang

Avalugg
Level: 61
Hardy Nature
Ability: Own Tempo
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Avalanche
- Crunch
- Earthquake
- Body Press

Kingambit
Level: 61
Hardy Nature
Ability: Supreme Overlord
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Iron Head
- Kowtow Cleave
- Zen Headbutt
- Stone Edge

Glimmora
Level: 62
Hardy Nature
Ability: Toxic Debris
EVs: 252 HP
IVs: 30 HP / 30 Atk / 30 Def / 30 SpA / 30 SpD / 30 Spe
- Tera Blast
- Sludge Wave
- Earth Power
- Dazzling Gleam
```

## Clavell - Level Cap 60/56
Ace is always the third evolution of the third starter as determined by your starter and will replace Houndoom, Gyrados, or Amoonguss depending on type
```
Oranguru
Level: 60
Hardy Nature
Ability: Inner Focus
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Yawn
- Dream Eater
- Reflect
- Foul Play

Abomasnow
Level: 60
Hardy Nature
Ability: Snow Warning
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Aurora Veil
- Blizzard
- Wood Hammer
- Ice Shard

Polteageist
Level: 60
Hardy Nature
Ability: Weak Armor
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Shell Smash
- Shadow Ball
- Sucker Punch
- Will-O-Wisp

Gyarados
Level: 60
Hardy Nature
Ability: Intimidate
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Aqua Tail
- Crunch
- Stone Edge
- Earthquake

Houndoom
Level: 60
Hardy Nature
Ability: Flash fire
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dark Pulse
- Fire Blast
- Sludge Bomb
- Thunder Fang

Amoonguss
Level: 60
Hardy Nature
Ability: Effect Spore
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Spore
- Toxic
- Giga Drain
- Hex

Meowscarada
Level: 61
Hardy Nature
Ability: Overgrow
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Shadow Claw
- Flower Trick
- Thunder Punch
- Play Rough

Quaquaval
Level: 61
Hardy Nature
Ability: Torrent
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Brick Break
- Aqua Step
- Aerial Ace
- Ice Spinner

Skeledirge
Level: 61
Hardy Nature
Ability: Blaze
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Shadow Ball
- Torch Song
- Earth Power
- Snarl
```

## Arven Fight 2- Level Cap 61/57
```
Greedent
Level: 58
Hardy Nature
Ability: Cheeck Pouch
IVs: 0 HP / 0 Atk / 0 Def / 0 SpA / 0 SpD / 0 Spe
- Bullet Seed
- Body Slam
- Psychic Fangs
- Earthquake

Cloyster
Level: 59
Hardy Nature
Ability: Skill Link
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Rock Blast
- Icicle Spear
- Liquidation
- Light Screen

Scovillain
Level: 60
Hardy Nature
Ability: Chlorophyll
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Fire Blast
- Energy Ball
- Zen Headbutt
- Crunch

Toedscruel
Level: 61
Hardy Nature
Ability: Mycellium Might
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Power Whip
- Earth Power
- Spore
- Sludge Bomb

Garganacl
Level: 62
Hardy Nature
Ability: Purifying Salt
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Stone Edge
- Earthquake
- Body Press
- Stealth Rock

Mabosstiff
Level: 63
Hardy Nature
Ability: Intimidate
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Crunch
- Psychic Fangs
- Fire Fang
- Play Rough
```

## Penny - Level Cap 62/58
```
Umbreon
Level: 62
Hardy Nature
Ability: Synchronize
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dark Pulse
- Quick Attack
- Baby-Doll Eyes
- Psychic

Vaporeon
Level: 62
Hardy Nature
Ability: Water Absorb
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Hydro Pump
- Quick Attack
- Baby-Doll Eyes
- Aurora Beam

Jolteon
Level: 62
Hardy Nature
Ability: Quick Feet
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Thunder
- Quick Attack
- Baby-Doll Eyes
- Pin Missle

Flareon
Level: 62
Hardy Nature
Ability: Flash fire
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Flare Blitz
- Quick Attack
- Baby-Doll Eyes

Leafeon
Level: 62
Hardy Nature
Ability: Leaf Guard
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Leaf Blade
- Quick Attack
- Baby-Doll Eyes
- X-Scizzor

Sylveon
Level: 63
Hardy Nature
Ability: Cute Charm
EVs: 252 HP
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Moonblast
- Quick Attack
- Baby-Doll Eyes
- Shadow Ball
```

## Nemona Fight 6 - Level Cap 65/61
```
Lycanroc
Level: 65
Hardy Nature
Ability: Sand Rush
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Accelerock
- Drill Run
- Stone Edge
- Stealth Rock

Goodra
Level: 65
Hardy Nature
Ability: Sap Sipper
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Dragon Pulse
- Muddy Water
- Ice Beam
- Sludge Bomb

Dudunsparce
Level: 65
Hardy Nature
Ability: Serene Grace
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Hyper Drill
- Drill Run
- Dragon Rush
- Coil

Orthworm
Level: 65
Hardy Nature
Ability: Earth Eater
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Iron Tail
- Body Press
- Earthquake
- Rock Blast

Pawmot
Level: 65
Hardy Nature
Ability: Volt Absorb
IVs: 20 HP / 20 Atk / 20 Def / 20 SpA / 20 SpD / 20 Spe
- Double Shock
- Close Combat
- Ice Punch
- Quick Attack

Skeledirge
Level: 66
Hardy Nature
Ability: Blaze
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Torch Song
- Earth Power
- Shadow Ball
- Snarl

Meowscarada
Level: 66
Hardy Nature
Ability: Overgrow
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Flower Trick
- Thunder Punch
- Shadow Claw
- Play Rough

Quaquaval
Level: 66
Hardy Nature
Ability: Torrent
EVs: 252 HP
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Aqua Step
- Brick Break
- Aerial Ace
- Ice Spinner
```

## The Way Home - Level Cap 66/62
You can access box or change teams during these fights but can't leave area zero once entered

#### Lab 1
```
Glimmora
Level: 62
Hardy Nature
- Sludge Wave
- Acid Armor
- Power Gem
- Rock Slide
```

#### Lab 2
```
Scream Tail
Level: 62
Hardy Nature
- Psychic Fangs
- Hyper Voice
- Play Rough
- Rest
```

#### Lab 3
```
Great Tusk
Level: 62
Hardy Nature
- Giga Impact
- Earthquake
- Knock Off
- Stomping Tantrum
```

#### Zero Lab
```
Great Tusk
Level: 64
Hardy Nature
- Close Combat
- Giga Impact
- Knock Off
- Earthquake

Brute Bonnet
Level: 64
Hardy Nature
- Spore
- Sucker Punch
- Giga Drain
- Thrash

Flutter Mane
Level: 64
Hardy Nature
- Psyshock
- Power Gem
- Mystical Fire
- Shadow Ball
```

## Professor Sada AI - Level Cap 67/63
```
Slither Wing
Level: 66
Hardy Nature
Ability: Protosynthesis
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Lunge
- Leech Life
- Low Sweep
- Zen Headbutt

Scream Tail
Level: 66
Hardy Nature
Ability: Protosynthesis
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Play Rough
- Drain Punch
- Ice Punch
- Zen Headbutt

Brute Bonnet
Level: 66
Hardy Nature
Ability: Protosynthesis
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Earth Power
- Giga Drain
- Payback
- Sucker Punch

Flutter Mane
Level: 66
Hardy Nature
Ability: Protosynthesis
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Power Gem
- Mystical Fire
- Shadow Ball
- Thunderbolt

Sandy Shocks
Level: 66
Hardy Nature
Ability: Protosynthesis
IVs: 25 HP / 25 Atk / 25 Def / 25 SpA / 25 SpD / 25 Spe
- Discharge
- Earth Power
- Flash Cannon
- Power Gem

Roaring Moon @ Booster Energy
Level: 67
Hardy Nature
Ability: Protosynthesis
IVs: 30 HP / 30 Atk / 30 Def / 30 SpA / 30 SpD / 30 Spe
- Dragon Claw
- Night Slash
- Stone Edge
- Earthquake
```


